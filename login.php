<?php

  session_start();

  if (isset($_SESSION['user_id'])) {
    header('Location: /php-login');
  }
  require 'database.php';

  if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $records = $conn->prepare('SELECT id, email, password, DNI, telefono FROM users WHERE email = :email');
    $records->bindParam(':email', $_POST['email']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $records->bindParam(':DNI', $_POST['DNI']);
    $records->bindParam(':telefono', $_POST['telefono']);

    $message = '';

    if (count($results) > 0 && password_verify($_POST['password'], $results['password'])) {
      $_SESSION['user_id'] = $results['id'];
      header("Location: /php-login");
    } else {
      $message = 'Los datos no son correctos';
    }
  }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Iniciar sesion</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
  </head>
  <body>
    <?php require 'partials/header.php' ?>

    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <h1>Iniciar sesion</h1>
    <span> o <a href="signup.php">Crear Cuenta</a></span>

    <form action="login.php" method="POST">
      <input name="email" type="text" placeholder="Enter your email" required="">
      <input name="password" type="password" placeholder="Enter your Password" required="">
      <input name="DNI" type="text" placeholder="Enter your DNI" required="">
      <input type="submit" value="Submit">
    </form>
  </body>
</html>
