<?php

  require 'database.php';

  $message = '';

  if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $sql = "INSERT INTO users (email, password, DNI, telefono) VALUES (:email, :password, :DNI, :telefono)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':email', $_POST['email']);
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':DNI', $_POST['DNI']);
    $stmt->bindParam(':telefono', $_POST['telefono']);

    if ($stmt->execute()) {
      $message = 'A creado un nuevo usuario';
    } else {
      $message = 'hubo un error al crear la cuenta';
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Crear Cuenta Nueva</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
  </head>
  <body>

    <?php require 'partials/header.php' ?>

    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <h1>Crear Cuenta Nueva</h1>
    <span> o <a href="login.php">Iniciar sesion</a></span>

    <form action="signup.php" method="POST">
      <input name="email" type="text" placeholder="Enter your email" required="">
      <input name="password" type="password" placeholder="Enter your Password" required="">
      <input name="confirm_password" type="password" placeholder="Confirm Password" required="">
      <input name="DNI" type="text" placeholder="Enter your DNI" required="">
      <input name="telefono" type="text" placeholder="Enter your telefono" required="">
      <input type="submit" value="Submit">
    </form>

  </body>
</html>
